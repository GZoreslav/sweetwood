<ol class="list-unstyled">
	<li><?php echo CHtml::link('Create New Post',array('post/create')); ?></li>
	<li><?php echo CHtml::link('Manage Posts',array('post/admin')); ?></li>
	<li><?php echo CHtml::link('Approve Comments',array('comment/index')) . ' (' . Comment::model()->pendingCommentCount . ')'; ?></li>
	<li><?php echo CHtml::link('Upload New Photo',array('photos/create')); ?></li>
	<li><?php echo CHtml::link('Manage Photos',array('photos/admin')); ?></li>
	<li><?php echo CHtml::link('Logout',array('site/logout')); ?></li>
</ol>

<?php
    $this->widget('bootstrap.widgets.TbMenu', array(
        'type'=>'list',
        'htmlOptions'=>array('class'=>'well small'),
        'items'=>array(
			array('label'=>'Posts'),
			array('label'=>'Update Photo', 'url'=>array('update', 'id'=>$model->id)),
			array('label'=>'Delete Photo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        ),
    ));
?>