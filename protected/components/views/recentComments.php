<div class="blog-recent-comments">
	<?php foreach($this->getRecentComments() as $comment): ?>
	<div>
		<span class="blog-commentshort-author"><?php echo $comment->authorLink; ?></span>
		<!-- <?php echo CHtml::link(CHtml::encode($comment->post->title), $comment->getUrl()); ?> -->
		<span class="blog-commentshort-text"><?php 
			echo CHtml::encode($comment->content); 
		?>
		</span>
		<span class="blog-commentshort-date"><?php echo date('F j, Y',$comment->create_time); ?></span>
	</div>
	<?php endforeach; ?>
</div>