<?php

Yii::import('zii.widgets.CPortlet');

class UserMenu extends CPortlet
{
	public function init()
	{
		//$this->title='You are logged in as ' . CHtml::encode(Yii::app()->user->name);
		parent::init();
	}

	protected function renderContent()
	{
		//$this->render('userMenu');
		    $this->widget('bootstrap.widgets.TbMenu', array(
        'type'=>'list',
        'htmlOptions'=>array('class'=>'well small'),
        'items'=>array(
			array('label'=>'Posts'),
			array('label'=>'Update Photo', 'url'=>array('update', 'id'=>$model->id)),
			array('label'=>'Delete Photo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        ),
    ));
	}
}