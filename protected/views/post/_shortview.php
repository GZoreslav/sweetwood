<div class="blog-post">
	<h2 class="blog-post-title">
		<?php echo CHtml::link(CHtml::encode($data->title), $data->url); ?>
	</h2>
	<p class="blog-post-meta"><?php echo 'posted on pon ' . date('F j, Y',$data->create_time); ?></p>
	<?php
		$this->beginWidget('CMarkdown', array('purifyOutput'=>true));
		echo substr($data->content, 0, 1024);
		echo '...';
		$this->endWidget();
	?>
	<div class="row">
		<div class="col-sm-8 blog-main blog-buttons-container">
			<?php echo CHtml::link('Read more', $data->url, array('class'=>'blog-readmore-button')); ?>
		</div>
		<div class="col-sm-3 col-sm-offset-1 blog-buttons-container text-right">
			<?php echo CHtml::link("Comments ({$data->commentCount})",$data->url.'#comments'); ?>
		</div>
	</div>
</div>
