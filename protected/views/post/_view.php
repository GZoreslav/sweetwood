<div class="blog-post">
	<h2 class="blog-post-title">
		<?php echo CHtml::link(CHtml::encode($data->title), $data->url); ?>
	</h2>
	<p class="blog-post-meta"><?php echo 'posted on pon ' . date('F j, Y',$data->create_time); ?></p>
			<?php echo 'prev:"' . $data->getPreviousId() . '"' ?>
			<?php echo 'next:"' . $data->getNextId() . '"' ?>
		<?php
			$this->beginWidget('CMarkdown', array('purifyOutput'=>true));
			echo $data->content;
			$this->endWidget();
		?>
	<div class="row">
		<div class="col-sm-12 blog-tags">
			<?php echo implode(' ', $data->tagLinks); ?>
		</div>
	</div>
</div>
