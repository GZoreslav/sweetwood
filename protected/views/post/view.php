<?php
$this->breadcrumbs=array(
	$model->title,
);
$this->pageTitle=$model->title;
?>

<?php $this->renderPartial('_view', array(
	'data'=>$model,
)); ?>

<div id="comments" class="blog-comments-block">
	<?php if($model->commentCount>=1): ?>
		<h3 class="blog-comments-block-title text-center">
			<?php echo $model->commentCount>1 ? $model->commentCount . ' responses to ' : 'One  response to '; ?>
			<?php echo '"' . $model->title . '"' ?>
		</h3>

		<?php $this->renderPartial('_comments',array(
			'post'=>$model,
			'comments'=>$model->comments,
		)); ?>
	<?php endif; ?>

	<h3 class="blog-comments-submit-title text-center">Leave a reply</h3>

	<?php if(Yii::app()->user->hasFlash('commentSubmitted')): ?>
		<div class="flash-success">
			<?php echo Yii::app()->user->getFlash('commentSubmitted'); ?>
		</div>
	<?php else: ?>
		<?php $this->renderPartial('/comment/_form',array(
			'model'=>$comment,
		)); ?>
	<?php endif; ?>

</div><!-- comments -->
