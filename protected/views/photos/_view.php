<?php
/* @var $this PhotosController */
/* @var $data Photos */
?>

<div class="view">

	<h1><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</h1>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('original_file_name')); ?>:</b>
	<?php echo CHtml::encode($data->original_file_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('file')); ?>:</b>
	<?php echo CHtml::encode($data->file); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />


</div>