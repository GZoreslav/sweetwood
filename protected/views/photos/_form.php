<?php
/* @var $this PhotosController */
/* @var $model Photos */
/* @var $form CActiveForm */
?>

<div role="form" class="blog-submit-form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'photos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="form-group">
        <?php echo $form->labelEx($model,'Photo'); ?>
        <?php echo CHtml::activeFileField($model, 'original_file_name',array('class'=>'form-control')); ?>
        <?php echo $form->error($model,'original_file_name'); ?>
	</div>

	<?php if($model->isNewRecord!='1'){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($model,'Old Photo'); ?>
	    <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$model->file,"image",array("width"=>750)); ?>
	</div>
	<?php } ?>

	<?php echo CHtml::submitButton($model->isNewRecord ? 'Upload' : 'Save', array('class'=>'btn btn-default')); ?>

<?php $this->endWidget(); ?>

</div><!-- form -->