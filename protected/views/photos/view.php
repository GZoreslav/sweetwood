<?php
/* @var $this PhotosController */
/* @var $model Photos */

$this->breadcrumbs=array(
	'Photoses'=>array('index'),
	$model->title,
);
?>

<?php
if (!Yii::app()->user->isGuest) {
    $this->widget('bootstrap.widgets.TbMenu', array(
        'type'=>'list',
        'stacked'=>'false',
        'htmlOptions'=>array('class'=>'well small'),
        'items'=>array(
			array('label'=>'Update Photo', 'url'=>array('update', 'id'=>$model->id)),
			array('label'=>'Delete Photo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        ),
    ));
}
?>

<h1><?php echo $model->title; ?></h1>

<div class="form-group">
	<?php echo CHtml::label('URL to copy', 'url'); ?>
	<?php echo CHtml::textField('url', '!['.$model->title.']('.$model->file.')', array('class'=>'form-control')); ?>
</div>

<div class="form-group">
	<?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$model->file,"image",array("width"=>750)); ?>
</div>