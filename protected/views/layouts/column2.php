<?php $this->beginContent('/layouts/main'); ?>
<div class="container container-bg">

	<div class="row">
        <div class="span12">
		    <div class="blog-header">
		        <h1 class="blog-title"><?php echo CHtml::encode(Yii::app()->name); ?></h1>
		        <p class="blog-description">... because I'm cooking now!</p>
		    </div>
		</div>
	</div>

	<div class="row">
        <div class="span8 blog-main">
			<div id="content">
				<?php echo $content; ?>
			</div><!-- content -->
		</div>
        <div class="span3 offset1 blog-sidebar">

        	<div class="content">

        	<?php if(!Yii::app()->user->isGuest) { ?>
				<?php 
				if(!Yii::app()->user->isGuest) 
					//$this->title='You are logged in as ' . CHtml::encode(Yii::app()->user->name);
					$this->widget('bootstrap.widgets.TbMenu', array(
			        'type'=>'list',
            		'htmlOptions'=>array('class'=>'well'),
			        'items'=>array(
						array('label'=>'Posts'),
						array('label'=>'Create New', 'url'=>array('post/create')),
						array('label'=>'Manage', 'url'=>array('post/admin')),
						array('label'=>'Comments'),
						array('label'=>'Approve Comments ('.Comment::model()->pendingCommentCount.' new comments)', 'url'=>array('comment/index')),
						array('label'=>'Photos'),
						array('label'=>'Upload', 'url'=>array('photos/create')),
						array('label'=>'Manage', 'url'=>array('photos/admin')),
						'---',
						array('label'=>'Logout', 'url'=>array('site/logout')),
			        ),
			    ));
				?>
			<?php } ?>

            <div class="sidebar-module sidebar-module-inset blog-tags-block">
				<?php $this->widget('TagCloud', array(
					'maxTags'=>Yii::app()->params['tagCloudCount'],
				)); ?>
			</div>

            <div class="sidebar-module sidebar-module-inset">
				<?php $this->widget('RecentComments', array(
					'maxComments'=>Yii::app()->params['recentCommentCount'],
				)); ?>
			</div>

			<div class="blog-social-pages">
				<a target="_blank" href="#" class="social-page social-page-fb">Facebook</a>
				<a target="_blank" href="#" class="social-page social-page-instagram">Instagram</a>
				<a target="_blank" href="#" class="social-page social-page-pinterst">Pinterest</a>
				<a target="_blank" href="#" class="social-page social-page-rss">RSS</a>
			</div>

			</div>

		</div>
	</div>
</div>
<?php $this->endContent(); ?>