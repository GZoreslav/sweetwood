<?php
$this->breadcrumbs=array(
	$model->title,
);
$this->pageTitle=Yii::app()->name." - ".$model->title;
?>

<?php $this->renderPartial('_view', array(
	'data'=>$model,
)); ?>

<div id="comments" class="blog-comments-block">

	<div class="fb-comments" 
	data-href="<?php echo Yii::app()->getBaseUrl(true).$model->url ?>" 
	data-numposts="10" 
	data-colorscheme="light" 
	data-width="622" 
	data-version="v2.3"></div>

</div><!-- comments -->
