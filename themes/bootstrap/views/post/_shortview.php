<div class="blog-post">
	<?php if (!Yii::app()->user->isGuest) { ?>
		<p><span class="status<?php echo $data->status; ?>"><?php echo $data->status; ?></span></p>
	<?php } ?>
	<h2 class="blog-post-title">
		<?php echo CHtml::link(CHtml::encode($data->title), $data->url); ?>
	</h2>
	<?php if (!Yii::app()->user->isGuest) { ?>
		<p><strong>Post actions:</strong> (
			<?php 
				echo CHtml::link('Edit', array('update', 'id'=>$data->id)); 
				echo ",&nbsp";
				echo CHtml::link('Delete', '#', array('submit'=>array('delete','id'=>$data->id),'confirm'=>'Delete this post?')); 
			?>
		)</p>
	<?php } ?>	
	<p class="blog-post-meta"><span>posted on </span><?php echo date('F j, Y',strtotime($data->create_time)); ?></p>
	<?php
		$this->beginWidget('CMarkdown', array('purifyOutput'=>true));
		$content = explode("<read_more>", $data->content);
		echo $content[0];
		echo '...';
		$this->endWidget();
	?>
	<div class="row-fluid">
		<div class="span8 blog-main blog-buttons-container blog-sharebuttons-container">
			<p>Розповісти друзям:</p>
			<?php 
				$url = Yii::app()->getBaseUrl(true);
				$fb_url = "https://www.facebook.com/sharer.php?u=".$url.$data->url;
				preg_match_all('/\]\(\/images\/[^\)]+\)/i', $data->content, $result);
				preg_match_all('/\]\(([^\)]*)\)/i',$result[0][0], $result2);
				$pin_src = $url.$result2[1][0];
				$pin_url = "http://www.pinterest.com/pin/create/button/?url=".$url.$data->url."&description=".CHtml::encode($data->title)." on %23sweetwood_com_ua&media=".$pin_src;
			?>
			<a class="social-share social-fb-share" href="#" onclick="window.open('<?php echo $fb_url ?>', 'Share me', 'width=700, height=300'); return false;">Facebook</a>
			<a target="_blank" href="#" class="social-share social-instagram-share">Instagram</a>
			<a class="social-share social-pinterest-share" href="#" onclick="window.open('<?php echo $pin_url ?>','_blank', 'width=700, height=500'); return false;">Pin It!</a>
		</div>
		<div class="span3 offset1 blog-buttons-container text-right">
			<span class="blog-comments-link">
				<?php echo CHtml::link("Comments {$data->commentCount}",$data->url.'#comments'); ?>
			</span>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span3 blog-main blog-buttons-container">
			<?php echo CHtml::link('Read more', $data->url, array('class'=>'blog-readmore-button')); ?>
		</div>
	</div>
</div>
