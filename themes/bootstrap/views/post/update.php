<?php
$this->breadcrumbs=array(
	$model->title=>$model->url,
	'Update',
);
?>

<h2>Update: [<?php echo CHtml::encode($model->title); ?>]</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>