<?php
/* @var $this ArticlesController */
/* @var $model Articles */
/* @var $form CActiveForm */
?>

<?php /** @var BootActiveForm $form */
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'horizontalForm',
	    'type'=>'vertical',
		'enableAjaxValidation'=>false,
	    'htmlOptions'=>array('class'=>'blog-submit-form row-fluid'),
	));
?>

	<?php echo CHtml::errorSummary($model); ?>

	<?php echo $form->textFieldRow($model, 'title', array('class'=>'span12', 'labelOptions'=>array('label'=>'Title:'))); ?>
	<div class="control-group">
		<label class="control-label">Create Time:</label>
		<div class="controls">
			<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			    'model' => $model,
			    'attribute' => 'create_time',
			    'options' => array(
				    'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25"
			    ),
			    'htmlOptions' => array(
			        'size' => '10',         // textField size
			        'maxlength' => '10',    // textField maxlength
			    ),
			));
			?>
		</div>
	</div>
	<?php echo $form->textAreaRow($model, 'content', array('class'=>'span12', 'rows'=>20, 'labelOptions'=>array('label'=>'Text:'))); ?>
	<?php
	/*$this->widget(
		'application.extensions.Yii-WysiBB-master.WysiBBWidget', 
		array(
			'model'=>$model,
			'attribute'=>'content',
			//'buttons'=>'bold,italic,underline,|,img,|',
		)
	); */
	?> 
	<?php echo $form->textFieldRow($model, 'tags', array('class'=>'span12', 'labelOptions'=>array('label'=>'Теги:'))); ?>
	<div class="control-group">
		<label class="control-label">Status:</label>
		<div class="controls">
			<?php echo $form->dropDownList($model,'status',Lookup::items('PostStatus'), array('class'=>'form-control')); ?>
		</div>
	</div>

	<div class="form-actions">
	    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Save')); ?>
	</div>

<?php $this->endWidget(); ?>