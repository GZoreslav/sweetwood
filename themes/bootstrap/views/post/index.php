<?php
    $dataProvider->pagination->pageSize = 6;
    Yii::app()->clientScript->registerMetaTag("кулінарія, рецепти, фотографії, випічка, тісто, смак", "keywords");
?>

<?php if(!empty($_GET['tag'])): ?>
    <h4 class="blog-tag-search-header">Posts Tagged with <i><?php echo CHtml::encode($_GET['tag']); ?></i></h4>
<?php endif; ?>

<?php if (!Yii::app()->user->isGuest) {
    $this->widget('bootstrap.widgets.TbMenu', array(
        'type'=>'list',
        'htmlOptions'=>array('class'=>'well'),
        'items'=>array(
            array('label'=>'Create New Post', 'icon'=>'plus', 'url'=>array('create')),
        ),
    ));
} ?>

<?php 
$this->widget('bootstrap.widgets.TbListView',array(
    'dataProvider'=>$dataProvider,
    'ajaxUpdate'=>false,
    'itemView'=>'_shortview',
    'template'=>"{items}\n{pager}",
)); 

    $cur_page = (int)($dataProvider->pagination->getCurrentPage()+1);
    if(!empty($_GET['tag'])) {
        $this->pageTitle=Yii::app()->name." - #".CHtml::encode($_GET['tag']);
    } else {
        $this->pageTitle=Yii::app()->name." - because I am cooking now...".($cur_page > 1 ? " | Сторінка ".$cur_page : "");
    }
    
    Yii::app()->clientScript->registerMetaTag("Вітаю у кулінарному блозі Sweetwood, де ділитимусь своїми улюбленими рецептами та фотографіями. Якщо Ви також одержуєте неймовірне задоволення, проводячи час на кухні, приєднуйтеся і смачного перегляду -:)".($cur_page > 1 ? " | Сторінка ".$cur_page : ""), "description");

?> 
