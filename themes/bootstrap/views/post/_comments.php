<?php foreach($comments as $comment): ?>
<div class="comment" id="c<?php echo $comment->id; ?>">

	<div class="blog-post-meta">
		<strong>
		<?php echo $comment->authorLink; ?></strong> replied on 
		<?php echo date('F j, Y \a\t h:i a',$comment->create_time); ?>
	</div>

	<div class="blog-comment-text-block">
		<span class="blog-comment-text">
			<?php echo nl2br(CHtml::encode($comment->content)); ?>
		</span>
	</div>

</div><!-- comment -->
<?php endforeach; ?>