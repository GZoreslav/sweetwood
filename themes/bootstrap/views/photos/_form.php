<?php
/* @var $this PhotosController */
/* @var $model Photos */
/* @var $form CActiveForm */
?>

<?php /** @var BootActiveForm $form */
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'horizontalForm',
	    'type'=>'vertical',
		'enableAjaxValidation'=>false,
		'htmlOptions' => array(
			'class'=>'row-fluid',
        	'enctype' => 'multipart/form-data',
    	),
	));
?>


	<?php echo $form->errorSummary($model); ?>
	<?php echo $form->textFieldRow($model, 'title', array('class'=>'span12', 'labelOptions'=>array('label'=>'Title:'))); ?>
	<label class="control-label required">Select image (622px width for 1 photo layout or 310px for 2 photos layout):</label>
	<div class="controls">
    	<?php echo CHtml::activeFileField($model, 'original_file_name',array('class'=>'form-control')); ?>
	</div>
	<?php if($model->isNewRecord!='1'){ ?>
	<label class="control-label">Old image:</label>
	<div class="controls">
	    <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$model->file,"image",array()); ?>
	</div>
	<?php } ?>
	<div class="form-actions">
	    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Save')); ?>
	</div>

<?php $this->endWidget(); ?>