<?php
/* @var $this PhotosController */
/* @var $model Photos */

$this->breadcrumbs=array(
	'Photoses'=>array('index'),
	$model->title,
);
?>

	<?php if (!Yii::app()->user->isGuest) { ?>
				<p><strong>Photo actions:</strong> (
					<?php 
					echo CHtml::link('Edit', array('update', 'id'=>$data->id)); 
					?>
					,&nbsp
					<?php
					echo CHtml::link('Delete', '#', array('submit'=>array('delete','id'=>$data->id),'confirm'=>'Delete this post?')); 
					?>
				)</p>
	<?php } ?>		


<h1><?php echo $model->title; ?></h1>

<div class="form-group row-fluid">
	<?php echo CHtml::label('Copy to insert this foto into a post:', 'url'); ?>
	<?php echo CHtml::textField('url', '!['.$model->title.']('.Yii::app()->request->baseUrl.'/images/'.$model->file.')', array('class'=>'form-control span-12')); ?>
</div>

<div class="form-group">
	<?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$model->file,"image",array("width"=>750)); ?>
</div>