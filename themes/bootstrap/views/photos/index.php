<?php

if (!Yii::app()->user->isGuest) {
    $this->widget('bootstrap.widgets.TbMenu', array(
        'type'=>'list',
        'htmlOptions'=>array('class'=>'well'),
        'items'=>array(
            array('label'=>'Upload new photo', 'icon'=>'plus', 'url'=>array('create')),
        ),
    ));
} ?>

<?php
    function renderImageWithCopyText($data)
    {
        $title = "<h3>".$data->title."</h3>";
        $image = CHtml::image(Yii::app()->request->baseUrl."/images/".$data->file,"image",array());
        $label = CHtml::label('Copy to insert this foto into a post:', 'url');
        $copyBlock = CHtml::textField('url', '!['.$data->title.']('.Yii::app()->request->baseUrl.'/images/'.$data->file.')', array('class'=>'form-control span6'));
        return $title.$label.$copyBlock."<br/>".$image;
    }
?>


<h1>Photos</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'hideHeader'=>false,
    'dataProvider'=>$dataProvider,
    'template'=>"{items}\n{pager}",
    'columns'=>array(
        array('header'=>'#', 'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'),
        array('value' => 'renderImageWithCopyText($data);', 'type'=>'raw', 'header'=>'Зображення'),
        //array('value' => 'CHtml::image(Yii::app()->request->baseUrl."/images/".$data->file,"image",array());', 'type'=>'raw', 'header'=>'Зображення 2'),
        array('class'=>'bootstrap.widgets.TbButtonColumn', 'visible'=>(!Yii::app()->user->isGuest), 'header'=>'Actions'),

    )
));

?>
