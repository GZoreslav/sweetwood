<?php
$this->breadcrumbs=array(
	'Comments',
);
?>

<h1>Comments</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'template'=>"{items}\n{pager}",
	'htmlOptions'=>array('class'=>'row-fluid'),
)); ?>
