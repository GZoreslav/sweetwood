<?php
$deleteJS = <<<DEL
$('.comment-admin').on('click','.time a.delete',function() {
	var th = $(this);
	if(confirm('Are you sure you want to delete this comment?')) {
		$.ajax({
			url:th.attr('href'),
			type:'POST'
		}).done(function(){th.parent().parent().slideUp()});
	}
	return false;
});
DEL;
Yii::app()->getClientScript()->registerScript('delete', $deleteJS);
?>
<div class="row-fluid comment-admin" id="c<?php echo $data->id; ?>">

	<div class="time span3">
		(<?php if($data->status==Comment::STATUS_PENDING): ?>
			<?php echo CHtml::linkButton('approve', array(
				'submit'=>array('comment/approve','id'=>$data->id),
			)); ?>, 
		<?php endif; ?>
		<?php echo CHtml::link('delete',array('comment/delete','id'=>$data->id),array('class'=>'delete')); ?> )
	</div>

	<div class="author span9">
		<span class="blog-commentshort-author"><?php echo $data->authorLink; ?></span><br>
		<strong>Date and time:</strong> <?php echo date('F j, Y \a\t h:i a',$data->create_time); ?><br>
		<strong>Comment:</strong> <?php echo nl2br(CHtml::encode($data->content)); ?><br>
		<strong>On post:</strong> <?php echo CHtml::link(CHtml::encode($data->post->title), $data->post->url); ?><br>
		<?php if($data->status==Comment::STATUS_PENDING): ?>
			<span class="pending">Pending approval</span>
		<?php endif; ?>
	</div>

</div><!-- comment -->