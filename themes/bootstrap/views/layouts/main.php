<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <meta property="fb:app_id" content="430883900321991" />
    <meta property='og:image' content='http://www.sweetwood.com.ua/themes/bootstrap/css/logo-08.png' />

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />

    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/wna.css">

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <?php Yii::app()->bootstrap->register(); ?>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="<?php echo Yii::app()->request->baseUrl; ?>assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Neucha&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Special+Elite' rel='stylesheet' type='text/css'>

</head>

<body class="body-bg">

<div id="fb-root"></div>

<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=430883900321991&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

    <!-- <?php $this->widget('bootstrap.widgets.TbNavbar',array(
        'collapse'=>true,
        'brand'=>'Home',
        'type'=>'inverse',
        'fixed' => true,
        'fluid' => false,
        'items'=>array(
            array(
                'class'=>'bootstrap.widgets.TbMenu',
                'items'=>array(
                    array('label'=>'About', 'url'=>array('site/page', 'view'=>'about')),
                    array('label'=>'Contact', 'url'=>array('site/contact')),
                    array('label'=>'Login', 'url'=>array('site/login'), 'visible'=>Yii::app()->user->isGuest),
                    array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('site/logout'), 'visible'=>!Yii::app()->user->isGuest)
                ),
            ),
        ),
        ));
    ?> -->

	<?php echo $content; ?>

    <!-- <nav class="navbar navbar-inverse blog-footer" role="navigation">
        <div class="container">
            Copyright &copy; <?php echo date('Y'); ?> by Nadya. All Rights Reserved.
        </div>
    </nav> -->

<script type="text/javascript">

    $(function (){
        var t = $('<div class="scroll-to-top-button"></div>');
        $('body').append(t);
        $(".scroll-to-top-button").hide();
        t.css({
            position: 'fixed',
            right: '10px',
            bottom: '10px',
            margin: 0,
            width: '80px'
        });
        $(window).scroll(function (){
            if ($(this).scrollTop() > 200){
                $(".scroll-to-top-button").fadeIn();
            } else{
                $(".scroll-to-top-button").fadeOut();
            }
        });
        $(".scroll-to-top-button").click(function (){
            $("body,html").animate({
                scrollTop:0
            }, 800);
            return false;
        });
    });
    
</script>

</body>
</html>