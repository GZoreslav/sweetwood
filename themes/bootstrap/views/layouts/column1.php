<?php $this->beginContent('/layouts/main'); ?>
<div class="container  container-bg">
    <div class="row">
        <div class="col-md-12">
            <div id="content">
		        <?php echo $content; ?>
            </div>
        </div>
	</div><!-- content -->
</div>
<?php $this->endContent(); ?>