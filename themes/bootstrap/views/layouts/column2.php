<?php $this->beginContent('/layouts/main'); ?>
<div class="container container-bg">

	<div class="row">
        <div class="span8">
		    <div class="blog-header">
		    	<a href="/index.php">
		    		<img src="/themes/bootstrap/css/logo-08.png" alt="sweetwood logo"/>
		    	</a>
		        <h1 class="blog-title"><?php echo CHtml::encode(Yii::app()->name); ?></h1>
		        <p class="blog-description">... because I'm cooking now!</p>
		    </div>
		</div>
		<div class="span4">
		    <!-- <div class="blog-main-menu">
		    	<?php echo CHtml::link('Home',array('post/index'), array('class'=>'menu-item')); ?>
		    	<?php echo CHtml::link('About',array('site/page', 'view'=>'about'), array('class'=>'menu-item')); ?>
		    	<?php echo CHtml::link('Contact',array('site/contact'), array('class'=>'menu-item')); ?>
		    	<?php if (Yii::app()->user->isGuest) {
		    		echo CHtml::link('Login',array('site/login'), array('class'=>'menu-item')); 
		    	} else {
		    		echo CHtml::link('Logout',array('site/logout'), array('class'=>'menu-item')); 
		    	} ?>
		    </div> -->
		</div>
	</div>

	<div class="row">
        <div class="span8 blog-main">
			<div id="content">
				<?php echo $content; ?>
			</div><!-- content -->
		</div>
        <div class="span4 blog-sidebar">

        	<div class="content">

        	<?php if(!Yii::app()->user->isGuest) { ?>
				<?php 
				if(!Yii::app()->user->isGuest) 
					//$this->title='You are logged in as ' . CHtml::encode(Yii::app()->user->name);
					$this->widget('bootstrap.widgets.TbMenu', array(
			        'type'=>'list',
            		'htmlOptions'=>array('class'=>'well'),
			        'items'=>array(
						array('label'=>'Posts', 'url'=>array('post/index')),
						array('label'=>'Comments ('.Comment::model()->pendingCommentCount.' new comments)', 'url'=>array('comment/index')),
						array('label'=>'Photos', 'url'=>array('photos/index')),
						array('---'),
						array('label'=>'Logout', 'url'=>array('site/logout')),
			        ),
			    ));
				?>
			<?php } ?>

            <div class="blog-author-info">
            	<div class="widget-content">
					<div><img class="author-logo" src="/themes/bootstrap/css/author-round.jpg"></div>
					<p>About...</p>
				</div>
			</div>

			<div class="blog-social-pages">
				<a target="_blank" href="#" class="social-page social-page-fb">Facebook</a>
				<a target="_blank" href="#" class="social-page social-page-instagram">Instagram</a>
				<a target="_blank" href="#" class="social-page social-page-pinterst">Pinterest</a>
				<a target="_blank" href="#" class="social-page social-page-rss">RSS</a>
			</div>

            <div class="sidebar-module sidebar-module-inset blog-tags-block">
				<?php $this->widget('TagCloud', array(
					'maxTags'=>Yii::app()->params['tagCloudCount'],
				)); ?>
			</div>

            <div class="sidebar-module sidebar-module-inset">
				<?php $this->widget('RecentComments', array(
					'maxComments'=>Yii::app()->params['recentCommentCount'],
				)); ?>
			</div>

			<img src="https://get.mycounter.ua/counter.php?id=148624" title="MyCounter" alt="MyCounter" width="1" height="1" border="0" style="opacity: 0;" />

			<div class="well" style="display: none;">
				<h5>Статистика</h5>
				<div class="panel-body">
					online: <span class="badge pull-right"><?php echo Yii::app()->userCounter->getOnline(); ?></span><br/>
					сьогодні: <span class="badge pull-right"><?php echo Yii::app()->userCounter->getToday(); ?></span><br/>
					вчора: <span class="badge pull-right"><?php echo Yii::app()->userCounter->getYesterday(); ?></span><br/>
					за весь час: <span class="badge pull-right"><?php echo Yii::app()->userCounter->getTotal(); ?></span><br/>
					максимум <?php echo date('d.m.Y', Yii::app()->userCounter->getMaximalTime()); ?>: <span class="badge pull-right"><?php echo Yii::app()->userCounter->getMaximal(); ?></span><br/>
				</div>
			</div>

			</div>

		</div>
	</div>
</div>
<?php $this->endContent(); ?>